
# `Internship 2019`
# Ссылки и ресурсы

Charlie Miller and Chris Valasek - тулзы и ресурсы по кархакингу

http://illmatics.com/carhacking.html

----

Уже известные пакеты в dbc формате

https://github.com/commaai/openpilot/blob/devel/opendbc/hyundai_kia_generic.dbc

----

Что такое dbc

http://socialledge.com/sjsu/index.php/DBC_Format

----

Описание dbc и файлы для конкретных автомобилей

https://github.com/commaai/opendbc

----

Таблица для заполнения

https://docs.google.com/spreadsheets/d/1dbU7b689YB4sELzwcrYCb1elXISWTFfvw0gpva5QxxY/edit?usp=sharing

----

Пример WEB приложения - задача №2

https://community.comma.ai/cabana/?route=53ac3251e03f95d7%7C2018-12-09--19-17-12&max=11&url=https%3A%2F%2Fchffrprivate.blob.core.windows.net%2Fchffrprivate3%2Fv2%2F53ac3251e03f95d7%2Fb2f9e71779148a59ffcda16921b4eb0e_2018-12-09--19-17-12


# Задача 1
Понять какие id шлет каждый модуль
# Задача 2
Разреверсить пакеты и понять как их слать, чтобы управлять модулями - блокировать двери, включать фары и подобное
# Задача 3
Создать WEB приложение для анализа dbc файла
# Задача 4
Для модели Hyundai Santa Fe существует файл hyundai_kia_generic.dbc. В этом файле не все пакеты, которые ходят по шине. Надо попытаться разреверсить пакеты, которых нет в этом файле и описать их формат
