#!/bin/bash

ttyACM=`find /dev/ -name 'ttyACM*' -print -quit`
while [[ -z $ttyACM ]];do
    echo "Plug the CAN-Hacker device into an USB socket and press ENTER"
    read enter
    ttyACM=`find /dev/ -name 'ttyACM*' -print -quit`
done

i=0

for ttyACM in $(ls /dev/ttyACM*)
do
    sudo slcand -o -c -s6 -S115200 $ttyACM can${i}
    sleep 1
    sudo ip link set up can${i}
    sudo ip link set can${i} txqueuelen 500
    if [[ $? -eq 0 ]]
    then
    	echo "Interface can${i} is ready"
    else
    	echo "Something wrong!!!"
    fi
    let "i++"
done
