#!/usr/bin/env python3
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--show_ids", action='store_true', help="print all ids in dump")
parser.add_argument("full_dump_path")
parser.add_argument("disconnected_dump_path")
args = parser.parse_args()

full_path = args.full_dump_path
disconnected_path = args.disconnected_dump_path

with open(full_path, "r") as file_full, open(disconnected_path, "r") as file_disconnected:
    lines_full = file_full.readlines()
    lines_disconnected = file_disconnected.readlines()

ignored_ids = {"7E0", "7E8", "7Ef"}


def split_by_cans(lines):
    can_ids = {}
    for line in lines:
        splitted_line = line.split()
        can, packet_id = splitted_line[1], splitted_line[2].split('#')[0]
        if can not in can_ids:
            can_ids[can] = set()
        can_ids[can].add(packet_id)
    for can in can_ids:
        can_ids[can] -= ignored_ids
    return can_ids


splitted_cans_full = split_by_cans(lines_full)
splitted_cans_disconnected = split_by_cans(lines_disconnected)

cans_set_full = set(splitted_cans_full.keys())
cans_set_disconnected = set(splitted_cans_disconnected.keys())

shared_buses = cans_set_full.intersection(cans_set_disconnected)

if args.show_ids:
    for bus in sorted(shared_buses):
        print("{} full: ".format(bus) + ' '.join(sorted(splitted_cans_full[bus])))
        print("{} disconnected: ".format(bus) + ' '.join(sorted(splitted_cans_disconnected[bus])))
    print()

if cans_set_full != shared_buses or cans_set_disconnected != shared_buses:
    print("Warning! Dumps have different cans recording! Will compare cans that present in both dumps.\n"
          "full dump cans: {}\ndisconnected dump cans: {}".format(' '.join(cans_set_full),
                                                                  ' '.join(cans_set_disconnected)))


for bus in sorted(shared_buses):
    print("{} ".format(bus), end='')

    disconnected_unique_ids = sorted(splitted_cans_disconnected[bus] - splitted_cans_full[bus])
    full_unique_ids = sorted(splitted_cans_full[bus] - splitted_cans_disconnected[bus])

    if len(disconnected_unique_ids) > 0:
        print("Warning! Some ids from disconnected dump are absent in full dump!\n"
              "May be you specified dump paths in wrong order?")
        print("Unique ids for disconnected dump:")
        print(' '.join(disconnected_unique_ids))

    print("missing ids: ", end='')
    print(' '.join(full_unique_ids))
