import argparse
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter#process_pdf
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
import io
import re
import xlsxwriter

class Module:
    name = str()
    description = str()
    def __str__(self):
        return self.name + self.description

def convert_pdf_to_txt(path_to_file):
    rsrcmgr = PDFResourceManager()
    retstr = io.StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = open(path_to_file, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos=set()

    # for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=True):
    #     interpreter.process_page(page)
    pages_list = list()
    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching,
                                  check_extractable=True):
        pages_list.append(page)
    interpreter.process_page(pages_list[1])
    text = retstr.getvalue()

    fp.close()
    device.close()
    retstr.close()
    return text


def diff(full_path, disconnected_path):


    with open(full_path, "r") as file_full, open(disconnected_path, "r") as file_disconnected:
        lines_full = file_full.readlines()
        lines_disconnected = file_disconnected.readlines()

    lines_full = sorted(set([x.split()[2].split('#')[0] for x in lines_full]))
    lines_disconnected = sorted(set([x.split()[2].split('#')[0] for x in lines_disconnected]))

    disconnected_unique_ids = list(set(lines_disconnected) - set(lines_full))
    full_unique_ids = list(set(lines_full) - set(lines_disconnected))

    if len(disconnected_unique_ids) > 0:
        print("Warning! Some ids in dump {} are absent in {}! "
              "May be you specified dump paths in wrong order?".format(disconnected_path, full_path))
        print("Unique ids for {}:".format(disconnected_path))
        print(', '.join(disconnected_unique_ids))

    print("Unique ids for {}:".format(full_path))
    return full_unique_ids


def get_modules_from_txt(filename):
    file = open(filename, "r")
    is_name = 1
    line = " "

    temp_list = list()

    while (line != ".\n"):
        line = file.readline()
        # print(line)
        if (line != ".\n"):
            new_module = Module()
            new_module.name = line
            temp_list.append(new_module)

    i = 0
    temp_str = ""
    while (line):
        if (line == "\n"):
            module = temp_list[i]
            module.description = temp_str
            # print("description ", temp_str)
            temp_str = ""
            i = i + 1
        line = file.readline()
        temp_str = temp_str + line

    return temp_list


def find_description_by_name(lst_modulus, name):
    for every in lst_modulus:
        if name == every.name:
            return every.description

def create_set_from_log(filename):
    set_ids = set()
    file = open(filename, "r")
    line=" "
    while line:
        line = file.readline()
        if line!="":
            splitted_line = line.split(" ")
            set_ids.add(splitted_line[1]+" "+splitted_line[2].split("#")[0])
    return set_ids



#find all modules which were disconnected
import os
table = dict()
modules = get_modules_from_txt("descriptions.txt")
entries = os.listdir('./dumps/')
print(entries)
regex = re.compile('.{1,30}log')
for entry in entries:
    if regex.match(entry):
        # print(entry.split(".")[0])
        set1 = create_set_from_log("./dumps/probably-full.log")
        set2 = create_set_from_log("./dumps/"+entry)
        difference = set1 - set2
        print("difference ", difference)
        if difference:
            for i in difference:
                can = i.split(" ")[0]
                module_name = entry.split(".")[0]
                description_module = find_description_by_name(modules, module_name+"\n")
                hex_id = i.split(" ")[1]
                dec_id = int(hex_id, 16)
                if (can, module_name) not in table:
                    table[(can, module_name)] = list()
                table[can, module_name].append((hex_id, dec_id))






def create_table(table, tableName):
    workbook = xlsxwriter.Workbook(tableName + '.xlsx')
    merge_format = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow'})

    text_format = workbook.add_format({
        'align': 'center',
        'valign': 'vcenter'
    })

    text_format.set_text_wrap()

    worksheet = workbook.add_worksheet()
    worksheet.merge_range('A1:F1', tableName, merge_format)
    worksheet.write('A2', 'No')
    worksheet.write('B2', 'Module')
    worksheet.write('C2', 'ECU')
    worksheet.write('D2', 'ID(hex)')
    worksheet.write('E2', 'ID(dec)')
    i=1
    index = 1
    print("table")
    print(table)
    for every in table:
        worksheet.write('A'+str(i+2), index, merge_format)
        worksheet.merge_range('A' + str(i + 2) + ':A' + str(i + len(every[1]) + 6), index, text_format)
        worksheet.merge_range('B'+str(i+2)+':B'+str(i+len(every[1])+6), every[0], text_format)
        worksheet.merge_range('C'+str(i+2)+':C'+str(i+len(every[1])+6), str(find_description_by_name(modules,every[0]+"\n")), text_format)
        index = index + 1
        another_index = 0
        for j in every[1]:
            worksheet.write('D' + str(another_index+i+2), j[0])
            worksheet.write('E' + str(another_index+i+2), j[1])
            another_index = another_index + 1
        i=i+5+len(every[1])

    worksheet.set_column('A:F', 25)
    worksheet.set_column('C:C', 70)
    workbook.close()

canC = [(k2, v) for (k1, k2), v in table.items() if k1 =="canC"]
canP = [(k2, v) for (k1, k2), v in table.items() if k1 =="canP"]
canB = [(k2, v) for (k1, k2), v in table.items() if k1 =="canB"]
canI = [(k2, v) for (k1, k2), v in table.items() if k1 =="canI"]

create_table(canC, "C-CAN")
create_table(canP, "P-CAN")
create_table(canB, "B-CAN")
create_table(canI, "I-CAN")









