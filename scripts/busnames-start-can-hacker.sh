#!/bin/bash

# Скрипт вместо can0, can1, can2, can3 дает имена canB, canC, canI, canP
# Шины в хаб подключать слева направо в алфавитном порядке
#
# Информация по пакетам при заведенном движке для проверки соответствия Интерфейс <-> Шина
# canB: в начале пакеты 100 101 102 103, после паузы все исчезло
# canC: в начале пакеты 7F 356 366, после паузы 470 остался
# canI: в начале много пакетов, после паузы остались 42 43 111
# canP: в начале много пакетов, после паузы 111 112 113

ttyACM=`find /dev/ -name 'ttyACM*' -print -quit`
while [[ -z $ttyACM ]];do
    echo "Plug the CAN-Hacker device into an USB socket and press ENTER"
    read enter
    ttyACM=`find /dev/ -name 'ttyACM*' -print -quit`
done

i=0
buses=(B C I P)

for ttyACM in $(ls /dev/ttyACM*)
do
    if (($i < 4)); then
      cur_bus=${buses[$i]}
    else
      echo "Can not have more than 4 buses"
      exit 0;
    fi

    sudo slcand -o -c -s6 -S115200 $ttyACM can${cur_bus}
    sleep 1
    sudo ip link set up can${cur_bus}
    sudo ip link set can${cur_bus} txqueuelen 500
    if [[ $? -eq 0 ]]
    then
    	echo "Interface can${cur_bus} is ready"
    else
    	echo "Something is wrong :("
    fi
    let "i++"
done
